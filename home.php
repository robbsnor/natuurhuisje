<?php
/*
 * Template Name: Homepage
 */
get_header(); ?>

<div class="hero cover" data-parallax="scroll" data-image-src="<?php the_field('hero_afbeelding'); ?>" style="background-image: url('<?php the_field('hero_afbeelding'); ?>')">
    <div class="gradient"></div>
    <div class="container-fluid h-100 ">
        <div class="row h-100">
            <div class="col-12 col-xl-6 text-center">
                <img src="<?php the_field('logo_van_natuurhuisje'); ?>" alt="" class="hero__logo">
            </div>
            <div class="col text-white">
                <h1>
                    <?php the_field('hero_tekst'); ?>
                </h1>
                <!-- <button type="button" class="btn btn-secondary" id="scrolldown">Bekijk de overnachting!</button> -->
            </div>
        </div>
    </div>
    <svg class="swirl" width="1920" height="123" viewBox="0 0 1920 123" preserveAspectRatio="none" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M962.5 39.5C533.5 6 115 68 -1.5 100V123H1919.5V0C1728 43.3333 1385 72.4924 962.5 39.5Z" fill="white" />
    </svg>
</div>

<main>
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-7">
                <div class="the-slider" id="scrollhere">
                    <div class="swiper-container">
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper">
                                <?php
                                //Get the images ids from the post_metadata
                                $images = acf_photo_gallery('slider_afbeeldingen', $post->ID);
                                //Check if return array has anything in it
                                if (count($images)) :
                                    //Cool, we got some data so now let's loop over it
                                    foreach ($images as $image) :
                                        $id = $image['id']; // The attachment id of the media
                                        $title = $image['title']; //The title
                                        $caption = $image['caption']; //The caption
                                        $full_image_url = $image['full_image_url']; //Full size image url
                                        // $full_image_url = acf_photo_gallery_resize_image($full_image_url, 262, 160); //Resized size to 262px width by 160px height image url
                                        $thumbnail_image_url = $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
                                        $url = $image['url']; //Goto any link when clicked
                                        $target = $image['target']; //Open normal or new tab
                                        $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
                                        $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
                                ?>
                                        <div class="swiper-slide" style="background-image:url(<?php echo $full_image_url; ?>)"></div>
                                <?php endforeach;
                                endif;
                                ?>
                            </div>
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div>

                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                <?php
                                //Get the images ids from the post_metadata
                                $images = acf_photo_gallery('slider_afbeeldingen', $post->ID);
                                //Check if return array has anything in it
                                if (count($images)) :
                                    //Cool, we got some data so now let's loop over it
                                    foreach ($images as $image) :
                                        $id = $image['id']; // The attachment id of the media
                                        $title = $image['title']; //The title
                                        $caption = $image['caption']; //The caption
                                        $full_image_url = $image['full_image_url']; //Full size image url
                                        // $full_image_url = acf_photo_gallery_resize_image($full_image_url, 262, 160); //Resized size to 262px width by 160px height image url
                                        $thumbnail_image_url = $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
                                        $url = $image['url']; //Goto any link when clicked
                                        $target = $image['target']; //Open normal or new tab
                                        $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
                                        $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
                                ?>
                                        <div class="swiper-slide" style="background-image:url(<?php echo $full_image_url; ?>)"></div>
                                <?php endforeach;
                                endif;
                                ?>
                            </div>
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div>
                    </div>
                </div>

                <?php the_field('beschrijving'); ?>
                <hr>

                <?php the_field('duurzaamheid_titel'); ?>
                <div class="duurzaamheid">
                    <div class="duurzaamheid__block">
                        <div class="duurzaamheid__icon">
                            <svg width="27" height="26" viewBox="0 0 27 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M17.4904 7.72063C12.4545 4.12809 7.24227 6.70706 2.42667 1.07612C1.30492 -0.237763 1.95644 15.8104 7.98444 21.8918C12.3916 26.334 19.1963 25.8385 21.4908 21.5214C23.7853 17.2044 22.5264 11.3125 17.4904 7.72063Z" stroke="#202020" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M9.29102 12.6365C14.1887 17.8458 19.2573 20.9329 25.9204 22.0905" stroke="#202020" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </div>
                        <div class="duurzaamheid__text">
                            <?php the_field('duurzaamheid_tekst_1'); ?>
                        </div>
                    </div>
                    <div class="duurzaamheid__block">
                        <div class="duurzaamheid__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2">
                                <polyline points="3 6 5 6 21 6"></polyline>
                                <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                <line x1="10" y1="11" x2="10" y2="17"></line>
                                <line x1="14" y1="11" x2="14" y2="17"></line>
                            </svg>
                        </div>
                        <div class="duurzaamheid__text">
                            <?php the_field('duurzaamheid_tekst_2'); ?>
                        </div>
                    </div>
                    <div class="duurzaamheid__block">
                        <div class="duurzaamheid__icon">
                            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.9277 21.3313V25.0001" stroke="#202020" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M12.9277 10.3257V13.9938" stroke="#202020" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M12.9277 1.15381V2.98821" stroke="#202020" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M21.316 10.3252H4.8866C4.77637 10.3253 4.66722 10.3015 4.56538 10.2554C4.46355 10.2093 4.37103 10.1416 4.29311 10.0564L1.18359 6.65676L4.29311 3.25711C4.37103 3.17187 4.46355 3.10425 4.56538 3.05812C4.66722 3.01199 4.77637 2.98826 4.8866 2.98828H21.316C21.5385 2.98828 21.7519 3.08491 21.9092 3.2569C22.0665 3.42889 22.1549 3.66216 22.1549 3.9054V9.40811C22.1549 9.65135 22.0665 9.88462 21.9092 10.0566C21.7519 10.2286 21.5385 10.3252 21.316 10.3252Z" stroke="#202020" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M4.53905 21.3315H20.9685C21.0787 21.3315 21.1879 21.3078 21.2897 21.2616C21.3915 21.2155 21.484 21.1479 21.562 21.0626L24.6715 17.6627L21.562 14.2628C21.484 14.1775 21.3915 14.1099 21.2897 14.0637C21.1879 14.0176 21.0787 13.9939 20.9685 13.9939H4.53905C4.31657 13.9939 4.1032 14.0905 3.94589 14.2625C3.78857 14.4345 3.7002 14.6678 3.7002 14.9111V20.4143C3.7002 20.6575 3.78857 20.8908 3.94589 21.0629C4.1032 21.2349 4.31657 21.3315 4.53905 21.3315Z" stroke="#202020" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg>
                        </div>
                        <div class="duurzaamheid__text">
                            <?php the_field('duurzaamheid_tekst_3'); ?>
                        </div>
                    </div>
                </div>
                <hr class="hr-2">
            </div>
            <div class="col-12 col-xl-5">
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl-12">
                        <?php the_field('details'); ?>
                        <hr class="hr-1">
                    </div>
                    <div class="col-12 col-lg-6 col-xl-12">
                        <div class="position-relative">
                            <img src="<?php echo get_template_directory_uri(); ?>/static/noot.png" alt="" class="nuts">
                        </div>

                        <?php the_field('faciliteiten_titel'); ?>
                        <div class="row">
                            <div class="col">
                                <?php the_field('faciliteiten'); ?>
                            </div>
                            <div class="col">
                                <?php the_field('faciliteiten_2'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <?php the_field('google_maps_tekst'); ?>
                <a href="<?php the_field('google_maps'); ?>" target="_blank" class="google-maps cover" style="background-image: url('<?php the_field('google_maps_afbeelding'); ?>')">
                    <button class="btn btn-secondary"><?php the_field('google_maps_button_tekst'); ?></button>
                </a>
            </div>
        </div>
    </div>
</main>

<footer class="footer">
    <div class="footer__bg cover" style="background-image: url('<?php the_field('reserveren_afbeelding'); ?>')"></div>
    <img src="<?php echo get_template_directory_uri(); ?>/static/erik.png" alt="" class="erik">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col col-xl-6">
                <div class="reserveren">
                    <?php the_field('reserveren_tekst'); ?>
                    <a href="<?php the_field('reserveren_link'); ?>" target="_blank" class="reserveren__btn btn btn-primary"><?php the_field('reserveren_knop_tekst'); ?></a>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <a href="<?php the_field('instagram_link'); ?>" target="_blank" class="btn btn-secondary instagram">
                <span><?php the_field('instagram_tekst'); ?></span>
                <div class="instagram__icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-instagram">
                        <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                        <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                        <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                    </svg>
                </div>
            </a>
        </div>
    </div>
</footer>


<?php get_footer(); ?>